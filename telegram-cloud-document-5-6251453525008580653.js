/// Comparasion Operators
/*
	what is difference between == and === ?
	Explain:
		== is comparing on value no matter type of properties
			for ex: 7 == '7' -> true
					true == 1 -> true
					...
		=== is comparing on both type and value
			for ex: 7 === '7' -> false
					true === 1 -> false
					'7' === '7' -> true
	Usecase:
		Prefer === when you wanna make sure that both type and value are the equal
		Prefer == when you just care about the value only no matter type of them
		in fact, it depends on usecase to decide which one will be prefered, 
		you should do practice more to get more familiar.
*/

/// Scope
/*
	Global - Local

*/

/// Variable definition
/*
	let - var - const
	What's the difference? How to use?
*/


/// Array: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array


/// SET: 
/// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set

/// WEAKSET - Avoid memory leak because the element will not get released/deallocated

/// MAPS
/// More Details: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map

/// WEAKMAP - Similiar to WEAKSET on the purpose respect

/// Create Maps by using a with nested arrays
const heroes = new Map([['Clark Kent', 'Superman', 'Extend'], ['Bruce Wayne', 'Betman']]);

/// List out all Maps's values
/// heroes.keys() returns an Iterator instead of Array, 
/// so we need to convert to Array to loop through each element
Array.from(heroes.keys()).forEach(element => {
    console.log(heroes.get(element));
});

/// Another way to list out
heroes.forEach((value, key) => {
    console.log(key);
    console.log(value);
});

/*
    Compare Maps and Object:
        . Key type: 
            + Maps: Any Type
            + Object: Only string
        . Get Size:
            + Maps: using `size` property
            + No efficient way, need to manually count -> Object.keys(obj).length
        . Purpose:
            + Maps: Focus on the storage and retrieval of key-value pairs.
            + Object: flexible, almost scenarios need it -> preferrable
        . JSON support:
            + Maps: No
            + Object: Yes
        . Access Properties:
            + Maps: using `get()` method
            + Object: Directly, obj.key, obj[key]
        ...
*/


/// Functions:
/// Every function has a special variable called `arguments` 
/// that is an array-like object containing every arg passed to the function
function arguments(){
    return arguments;
}

let args = arguments(1, 2, 3, 4, 5);
console.log(args);
/// Result: { '0': 1, '1': 2, '2': 3, '3': 4, '4': 5 }
/// Access each element by using index natation like we did with arrays but it's not an array, wth.
args[0];

/// Using `rest` is better
/// The args parameter is an actual array
function rest(...args){
    /// Another way with loop: for-of
    for(arg of args){
        console.log(arg);
    }
}

/// Rounding Errors
function mean(a,b,c){
    return (a+b+c)/3;
}

mean(1, 3, 6); /// OK
mean(1, 3); /// Error because the third argument is missing
mean(1, 3, 6, 5, 3); /// OK because the function will automatically take 3 first args and ignore the rest

/// Default Parameters
function hello(name='World') {
    console.log(`Hello ${name}!`);
}

/// Arrow functions
const square = x => x*x;


/// TBC....


