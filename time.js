// function time(d, h, m, s) {
//     if (d < 0 || h < 0 || m < 0 || s < 0) {
//         return "nhap d,h,m,s phai lon hon 0"
//     } else {
//         return s = d * 3600 * 24 + h * 3600 + m * 60 + s
//     }
// }
// console.log(time(1, 2, 3, 4));


function change(time) {
    return time.days * 3600 * 24 + time.hours * 3600 + time.minutes * 60 + time.seconds;
}
let time = {
    days: -1,
    hours: 2,
    minutes: 3,
    seconds: 4,
}
if (time.days < 0 || time.hours < 0 || time.minutes < 0 || time.seconds < 0) {
    console.log("thoi gian phai lon hon hoac bang 0");
    return false;
} else console.log(change(time));