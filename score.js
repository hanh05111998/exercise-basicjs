let score = [{
        studentName: "Phan Van Teo",
        math: 8.7,
        physical: 7.2,
        chemistry: 6.9,
        literature: 7.8,
        history: 7.6,
        geography: 9.1,

    },
    {
        studentName: "Ban Thi No",
        math: 9.2,
        physical: 6.5,
        chemistry: 7.0,
        literature: 6.3,
        history: 5.6,
        geography: 8.5,


    }, {
        studentName: "Ton That Hoc",
        math: 10,
        physical: 5.7,
        chemistry: 10,
        literature: 10,
        history: 9.5,
        geography: 9.8,

    }
]

function average(dtb) {
    let arr = Object.keys(dtb)
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        if(typeof dtb[arr[i]] ==='number'){
        sum += dtb[arr[i]]
        }
    }
    return sum / arr.length;
}
let max = 0;
let index = 0
for (let i = 0; i < score.length; i++) {
    if (max < average(score[i])) {
        max = average(score[i])
        index = i
    }
}
console.log("hoc sinh gioi nhat la:"+score[index]['studentName']);
